from django_cron import CronJobBase, Schedule

from .synchronization import EnergyMeasurementSynchronizer


class MeasurementsSyncCronJob(CronJobBase):
    RUN_EVERY_MINS = 59
    schedule = Schedule(run_every_mins=RUN_EVERY_MINS)
    code = 'smi_unb.api.cronjob.MeasurementsSyncCronJob'

    def do(self):
        e_synchronizer = EnergyMeasurementSynchronizer()
        e_synchronizer.perform_all_measurements_sync()
