# -*- coding: utf-8 -*-
# Generated by Django 1.9.8 on 2017-05-22 21:36
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='userpermissions',
            options={'permissions': (('manager_buildings', 'Manager Buildings'), ('manager_transductors', 'Manager Transductors'))},
        ),
    ]
