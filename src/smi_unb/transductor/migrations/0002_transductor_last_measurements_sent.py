# -*- coding: utf-8 -*-
# Generated by Django 1.9.8 on 2017-06-07 01:21
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('transductor', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='transductor',
            name='last_measurements_sent',
            field=models.DateTimeField(blank=True, null=True),
        ),
    ]
