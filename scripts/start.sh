#!/bin/bash

# Exporting all environment variables to use in crontab
env | sed 's/^\(.*\)$/ \1/g' > /root/env

while ! pg_isready -h $DB_SERVICE -p $DB_PORT -q -U $DB_USER; do
  >&2 echo "Postgres is unavailable - sleeping..."
  sleep 1
done

# Populating database and Initializing cron and gunicorn
python3 manage.py makemigrations                                  && \
python3 manage.py migrate                                         && \
python3 manage.py loaddata src/smi_unb/fixtures/initial_data.json && \
cron                                                              && \
gunicorn smi_unb.wsgi -b 0.0.0.0:8000
